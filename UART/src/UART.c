/*
 * UART.c
 *
 *      Author: Embedownik
 */
#include <string.h>

#include <UART.h>

/*
 * Types of commands for main module task.
 */
typedef enum
{
    receivedData_receivedByte,
}receivedDataCommand_e;

/*
 * Struct with single command for main module task.
 */
typedef struct
{
    receivedDataCommand_e command;
    uint8_t receivedData;
}receivedQueueData_s;

/* static array for all instances */
static UART_config_s *configs[UART_MAX_INSTANCES];

/* counter for actual allocated module intances in static array */
static uint8_t allocatedUARTS = 0U;

/* internal module indicator - for errorCode */
static const uint8_t WRONG_INSTANCE_ID_VALUE = 255U;

/*
 * Start receiving - right now - sign by sign.
 */
static void startReceiver(UART_config_s *config)
{
    HAL_UART_Receive_IT(config->handler, &config->receiveBuff, 1U);
}

bool receiveFromCommandQueue(UART_config_s *config, receivedQueueData_s *data)
{
    bool retVal = false;

    if(xQueueReceive(config->receivedQueue, data, portMAX_DELAY) == pdPASS)
    {
        retVal = true;
    }

    return retVal;
}

void callbackRun_ReceiveByte(UART_config_s *config, uint8_t data)
{
    if(config->receiveByteCallback)
    {
        config->receiveByteCallback(data);
    }
}

/**
 * Common task function - the same for every UART instance - with different init parameter.
 * @param param pointer to UART_config_s with settings
 */
static void receiverTask(void *param)
{
    UART_config_s *myConfig = (UART_config_s *)param;
    receivedQueueData_s receivedData;

    while(true)
    {
        if(receiveFromCommandQueue(myConfig, &receivedData))
        {
            switch(receivedData.command)
            {
                case receivedData_receivedByte:
                {
                    if(myConfig->receiverLinesMode)
                    {
                        switch(receivedData.receivedData)
                        {
                            case '\n':
                            {
                                break;
                            }
                            case '\r':
                            {
                                if(myConfig->lineBufferPosition != 0U) /* skip empty lines */
                                {
                                    myConfig->lineBuffer[myConfig->lineBufferPosition] = '\0';

                                    /* run event callback */
                                    myConfig->receiveLineCallback(myConfig->lineBuffer);

                                    myConfig->lineBufferPosition = 0U;
                                }

                                break;
                            }
                            default:
                            {
                                myConfig->lineBuffer[myConfig->lineBufferPosition] = receivedData.receivedData;
                                myConfig->lineBufferPosition++;

                                /* in case of buffer overflow - just "clear" data */
                                /* TODO - add some error callback */
                                if(myConfig->lineBufferPosition == myConfig->lineBufferLength - 1U)
                                {
                                    myConfig->lineBufferPosition = 0U;
                                }
                            }
                        }
                    }
                    else
                    {
                        callbackRun_ReceiveByte(myConfig, receivedData.receivedData);
                    }

                    break;
                }
                default:
                {
                    break;
                }
            }
        }
    }
}

bool UART_Init(UART_config_s *config)
{
    bool initStatus = true;

    if(!config->initialized && allocatedUARTS < UART_MAX_INSTANCES)
    {
        configs[allocatedUARTS] = config;

        startReceiver(config);

        config->initialized = true;

        /* create receiving task for selected UART */
        BaseType_t xReturned = xTaskCreate(
                        receiverTask,                /* Function that implements the task. */
                        config->taskConfig.taskName, /* Text name for the task. */
                        config->taskConfig.stackSize,/* Stack size in words, not bytes. */
                        (void *)config,              /* Parameter passed into the task. */
                        config->taskConfig.priority, /* Priority at which the task is created. */
                        NULL);                       /* Used to pass out the created task's handle. */

        if(xReturned != pdPASS)
        {
            initStatus = false;
        }

        if(config->receiverEnabled)
        {
            config->receivedQueue = xQueueCreate(config->receivedQueueSize, sizeof(receivedQueueData_s));

            if(config->receivedQueue == NULL)
            {
               /* TODO - some kind of errors etc - but this should fire internal RTOS error handle about fail in allocation */
                while(true);
            }
        }

        allocatedUARTS++;
    }
    else
    {
        initStatus = false;
    }

    return initStatus;
}

void UART_TransmitBuff(UART_config_s *config, const uint8_t *buff, uint16_t len)
{
    while(config->transmitterBusyFlag)
    {
        vTaskDelay(1U);
    }

    config->transmitterBusyFlag = true;

    /* because of lack of consts ST HAL library :/ */
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdiscarded-qualifiers"
        HAL_UART_Transmit_IT(config->handler, buff, len);
#pragma GCC diagnostic pop

    /* right now just wait in blocking mode */
    while(config->transmitterBusyFlag)
    {
        vTaskDelay(1U);
    }
}

void UART_TransmitText(UART_config_s *config, const char *buff)
{
    const uint16_t maxTransmitLen = 1000U;

    uint16_t buffLen = strnlen(buff, maxTransmitLen);

    if(buffLen != maxTransmitLen)
    {
        UART_TransmitBuff(config, (const uint8_t *)buff, buffLen);
    }
}

static uint8_t checkHandleInstanceIndex(UART_HandleTypeDef *huart)
{
    uint8_t index = WRONG_INSTANCE_ID_VALUE;

    for(uint8_t i = 0; i < UART_MAX_INSTANCES; i++)
    {
        if(configs[i]->handler->Instance == huart->Instance)
        {
            index = i;
            break;
        }
    }

    return index;
}

static bool checkInstanceIndexIsValid(uint8_t index)
{
    bool retval = true;

    if(index == WRONG_INSTANCE_ID_VALUE)
    {
        retval = false;
    }

    return retval;
}

static void sendToReceiveQueueFromISR(UART_config_s *config, bool receiverIDLE, uint8_t sign)
{
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;

    receivedQueueData_s command;

    if(receiverIDLE)
    {
        /* removed feature - refactor later */
//        command.command = receivedData_IDLE;
    }
    else
    {
        command.command = receivedData_receivedByte;
        command.receivedData = sign;
    }

    xQueueSendFromISR(config->receivedQueue, &command, &xHigherPriorityTaskWoken);

    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

void UART_CallFrom_TxCpltCallback(UART_HandleTypeDef *huart)
{
    uint8_t instanceIndex = checkHandleInstanceIndex(huart);

    if(checkInstanceIndexIsValid(instanceIndex))
    {
        configs[instanceIndex]->transmitterBusyFlag = false;
    }
}

void UART_CallFrom_RxCpltCallback(UART_HandleTypeDef *huart)
{
    uint8_t instanceIndex = checkHandleInstanceIndex(huart);

    if(checkInstanceIndexIsValid(instanceIndex))
    {
        configs[instanceIndex]->receivedToRead = configs[instanceIndex]->receiveBuff;

        startReceiver(configs[instanceIndex]);

        configs[instanceIndex]->receivedFlag = true;

        sendToReceiveQueueFromISR(configs[instanceIndex], false, configs[instanceIndex]->receivedToRead);
    }
}

#ifndef UART_DONT_OVERRIDE_HAL_CALLBACKS

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
    UART_CallFrom_TxCpltCallback(huart);
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
    UART_CallFrom_RxCpltCallback(huart);
}

#endif
