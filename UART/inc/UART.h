/*
 * UART.h
 *
 *      Author: Embedownik
 */

#ifndef UART_UART_INC_UART_H_
#define UART_UART_INC_UART_H_

#include <stdint.h>
#include <stdbool.h>

#include <FreeRTOS.h>
#include <task.h>
#include <queue.h>

#include <board.h>

/*
 * Just test calculations:
 *
 * Core speed | baudrate | cycles | time
 *    16MHz   | 9600     | 16666  | ~1ms
 *    80MHz   | 115200   | 83330  | ~1ms
 *
 *    16MHz   | 115200   | 1388
 *    32MHz   | 115200   | 2776
 *    80MHz   | 115200   | 6940
 */

/* Callback for "byteReceived" event */
typedef void (*UART_receiveByteUserParser_t)(uint8_t data);

/*
 * Callback for "lineReceived" event.
 * End of line replaced with '\0'.
 */
typedef void (*UART_receiveLineCallback_t)(uint8_t *line);

typedef struct
{
    char    *taskName;
    uint8_t  priority;
    uint16_t stackSize;
}UART_config_RTOSTask_s;

typedef struct
{
    UART_HandleTypeDef *handler;

    UART_config_RTOSTask_s taskConfig;

    bool receiverEnabled;      /* enable receiver on selected UART */
    bool receiverLinesMode;    /* fires user received data in case of full line receive. "line end" sign is "\n" (10dec) */

    uint8_t *lineBuffer;       /* buffer for "reveiveLine" mode */
    uint8_t lineBufferLength;  /* length of lineBuffer - given by user */

    uint8_t receivedQueueSize; /* size of internal receive queue */

    UART_receiveByteUserParser_t receiveByteCallback; /* callback to call after received data event */
    UART_receiveLineCallback_t receiveLineCallback;   /* callback to call after received line event */


    /* Internal module parameters - this way to avoid dynamic allocation */
    uint8_t receiveBuff; /* TODO - better buff - right now just testing */

    uint8_t receivedToRead; /* todo better implementation */

    bool receivedFlag;
    bool initialized;

    bool volatile transmitterBusyFlag;

    uint16_t lineBufferPosition;

    QueueHandle_t receivedQueue;
}UART_config_s;

bool UART_Init(UART_config_s *config);

/**
 * Transmit text - required '/0' at the end.
 * Warning - blocking implementation!
 * @param config UART instance pointer
 * @param buff buff to send.
 */
void UART_TransmitText(UART_config_s *config, const char *buff);

/**
 * Transmit buffer with data.
 * Warning - blocking implementation!
 * @param config UART instance pointer
 * @param buff buff to send
 * @param len length of data to send
 */
void UART_TransmitBuff(UART_config_s *config, const uint8_t *buff, uint16_t len);

/**
 * Function to call from ST function void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
 * when UART_DONT_OVERRIDE_HAL_CALLBACKS is used.
 * @param huart - handler from HAL_UART_RxCpltCallback param
 */
void UART_CallFrom_RxCpltCallback(UART_HandleTypeDef *huart);

/**
 * Function to call from ST function void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
 * when UART_DONT_OVERRIDE_HAL_CALLBACKS is used.
 * @param huart - handler from HAL_UART_TxCpltCallback param
 */
void UART_CallFrom_TxCpltCallback(UART_HandleTypeDef *huart);

#endif
